(function(){
	'use strict';

	angular
		.module('LocaldataModule',[])
		.service('localdataService', ['$q', '$rootScope', '$timeout', localdataService]);
	
	function localdataService($q, $rootScope, $timeout){
		var self = this;

		/**
		 * Check if given key is avaialable in localstorage
		 * @param  {String} key Localstorage data key
		 * @return {Boolean}     True/Flase
		 */
		self.available = function(key){
			return (localStorage.getItem(key) != null);
		}

		/**
		 * Fetch required data from server
		 * @param  {Object}   model    instance of model
		 * @param  {Function}   getter   Function in model instance to query server
		 * @param  {Function} callback A callback function to return server query response
		 */
		self.fetch = function(model, getter, callback){
			model[getter]().then(function(state){
				var result = model.records;
				callback(result);
			}).catch(function(){
				callback();
			});
		}

		/**
		 * Save data to local storage
		 * @param  {String} key  data key against which data will be saved
		 * @param  {Object} data Actual data
		 */
		self.save = function(key, data){
			localStorage.setItem(key, JSON.stringify(data));
		}

		/**
		 * Read data from localstorage
		 * @param  {String} key localstorage data key
		 * @return {Object}     Required data
		 */
		self.read = function(key){
			return JSON.parse(localStorage.getItem(key));
		}

		/**
		 * Remove data from localstorage
		 * @param  {String} key localstorage data key
		 * @return {} 
		 */
		self.remove = function(key){
			return localStorage.removeItem(key);
		}
	}
})()