(function(){
	'use strict';

	angular
		.module('site')
		.factory('dataprovide', ['$q', '$timeout', 'localdataService', dataprovide]);

	function dataprovide($q, $timeout, localdataService){
		var factory = dataprovide;
		var data = new Object();

		function dataprovide(){
			data = localdataService.read('fakery');
			this.record = {};
			this.records = {};
			this.params = {};
			this.navigation = {};
			this.errors = {
				'has': {},
				'message': {}
			}
		}

		dataprovide.prototype.query = function(){
			var self = this,
				deferred = $q.defer(),
				result = new Array();
			
			$timeout(function(){
				for(var i=0; i<(self.params.page*5); i++){
					var item = data[i];
					if(typeof item !== 'undefined') result.push(item);
				}
				deferred.resolve(result);
			},500);
			

			return deferred.promise;
		}

		dataprovide.prototype.get = function(id){
			var self = this,
				result = new Object();
			
			for(var i=0; i<data.length; i++){
				var item = data[i];
				if(item._id == id) result = item;
			}

			return result;
		}

		dataprovide.prototype.addComment = function(id){
			var self = this;

			for(var i=0; i<data.length; i++){
				var item = data[i];
				if(item._id == id){
					var user = item.user;
					item.comments.unshift({
						'content': self.record,
						'user': user
					});
				}
			}

			localdataService.save('fakery', data);
		}

		return factory;
	}
})();