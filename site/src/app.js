(function(){
	'use strict'

	angular
		.module('site', ['ngRoute', 'LocaldataModule'])
		.config(['$routeProvider', '$locationProvider', config])
		.run(['$http', 'localdataService', run])
		.filter('length', function() {
			return function(input) {
				if(typeof input !== 'undefined')
				return input.length;
			}
		});

	function config($routeProvider, $locationProvider){
		$routeProvider
			.when('/', {
				controller: 'index',
				templateUrl: 'views/index.html',
			})
			.when('/detail?', {
				controller: 'detail',
				templateUrl: 'views/detail.html',
			});
		$locationProvider.html5Mode(true);
	}

	function run($http, localdataService){
		
		if(!localdataService.available('fakery')){
			$http({ 
				method: 'GET', 
				url: 'data.json'
			})
			.then(function(receive){
				localdataService.save('fakery', receive.data);
			})
			.catch(function(reason, cause){
				console.error(reason, cause);
			});
		}
	}
})();