(function(){
	'use strict';

	angular
		.module('site')
		.directive('scrollBottom', ['$rootScope', scrollBottom]);

	function scrollBottom($rootScope){
		var directive = new Object();

			directive.restrict = 'A';
			directive.scope = true;
			directive.link = link;

		function link(){
			var timeout,
				scrollpos = 0;
			document.addEventListener("scroll", function(){
				//console.log(window.scrollY + window.outerHeight, document.body.clientHeight);
				if(window.scrollY + window.outerHeight > document.body.clientHeight && scrollpos < window.scrollY) {
					clearTimeout(timeout);

					timeout = setTimeout(function(){
						$rootScope.$broadcast('scroll:bottom');
					},300);
				}
				scrollpos = window.scrollY;
			});
		}

		return directive;
	}
})();