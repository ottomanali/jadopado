(function(){
	'use strict'

	angular
		.module('site')
		.directive('jrange',['$timeout', jrange]);

	function jrange($timeout){
		var directive = {};
			directive.restrict = 'A';
			directive.scope = {
				'model': '='
			}
			directive.link = link;

		function link(scope, element, attributes){
			$timeout(function(){
				var range = strToRange(attributes.range);
				element.jRange({
					from: range.from,
					to: range.to,
				    step: 1,
				    width: 200,
				    showLabels: false,
				    showScale: false,
				    isRange : (attributes.jrange == 'true') ? true : false,
				    onstatechange: function(value){
				    	scope.$apply(function(){
				    		scope.model = value;
				    	});
				    },			
				});
				
				if(typeof scope.minmodel !== 'undefined' && typeof scope.maxmodel !== 'undefined'){ 
					var time;
					scope.$watch(watch, function(newValue, oldValue){
						clearTimeout(time);
						time = setTimeout(function() {
							element.jRange('setValue', newValue);
						}, 700);
					});

					function watch(){
						return scope.minmodel+','+scope.maxmodel;
					}
				}

			},1000);

		}

		function strToRange(value) {
			return {
				'from': value.split(',')[0],
				'to': value.split(',')[1]
			}
		}

		function rangeToStr(range) {
			return range.from+','+range.to;
		}

		return directive;
	}
})();