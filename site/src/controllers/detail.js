(function(){
	'use strict';

	angular
		.module('site')
		.controller('detail', ['$scope', '$routeParams', 'dataprovide', detail]);

	function detail($scope, $routeParams, dataprovide){
		$scope.item = new Object();
		$scope.form = new Object();
		$scope.dataprovide = new dataprovide();

		$scope.item = $scope.dataprovide.get($routeParams.id);
		

		$scope.submit = function(){
			$scope.dataprovide.record = $scope.comment;
			$scope.dataprovide.addComment($routeParams.id);
			$scope.comment = '';
		}
	}
})();