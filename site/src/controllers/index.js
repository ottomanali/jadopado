(function(){
	'use strict';

	angular
		.module('site')
		.controller('index', ['$scope', '$timeout', 'dataprovide', index]);

	function index($scope, $timeout, dataprovide){
		$scope.products = new Array();
		$scope.distance = 200;
		var page = 1,
			dataprovide = new dataprovide();

		dataprovide.params = {'page': page};
		dataprovide.query().then(function(data){
			$scope.products = data;
		});

		$scope.$on('scroll:bottom', function(){
			$timeout(function(){
				page++;
				dataprovide.params = {'page': page};
				$scope.products = dataprovide.query();
			});
		});
	}
})();