(function(){
	'use strict'

	angular
		.module('site', ['ngRoute', 'LocaldataModule'])
		.config(['$routeProvider', '$locationProvider', config])
		.run(['$http', 'localdataService', run])
		.filter('length', function() {
			return function(input) {
				if(typeof input !== 'undefined')
				return input.length;
			}
		});

	function config($routeProvider, $locationProvider){
		$routeProvider
			.when('/', {
				controller: 'index',
				templateUrl: 'views/index.html',
			})
			.when('/detail?', {
				controller: 'detail',
				templateUrl: 'views/detail.html',
			});
		$locationProvider.html5Mode(true);
	}

	function run($http, localdataService){
		
		if(!localdataService.available('fakery')){
			$http({ 
				method: 'GET', 
				url: 'data.json'
			})
			.then(function(receive){
				localdataService.save('fakery', receive.data);
			})
			.catch(function(reason, cause){
				console.error(reason, cause);
			});
		}
	}
})();
(function(){
	'use strict';

	angular
		.module('site')
		.controller('detail', ['$scope', '$routeParams', 'dataprovide', detail]);

	function detail($scope, $routeParams, dataprovide){
		$scope.item = new Object();
		$scope.form = new Object();
		$scope.dataprovide = new dataprovide();

		$scope.item = $scope.dataprovide.get($routeParams.id);
		

		$scope.submit = function(){
			$scope.dataprovide.record = $scope.comment;
			$scope.dataprovide.addComment($routeParams.id);
			$scope.comment = '';
		}
	}
})();
(function(){
	'use strict';

	angular
		.module('site')
		.controller('index', ['$scope', '$timeout', 'dataprovide', index]);

	function index($scope, $timeout, dataprovide){
		$scope.products = new Array();
		$scope.distance = 200;
		var page = 1,
			dataprovide = new dataprovide();

		dataprovide.params = {'page': page};
		dataprovide.query().then(function(data){
			$scope.products = data;
		});

		$scope.$on('scroll:bottom', function(){
			$timeout(function(){
				page++;
				dataprovide.params = {'page': page};
				$scope.products = dataprovide.query();
			});
		});
	}
})();
(function(){
	'use strict';

	angular
		.module('site')
		.directive('onFinishRender', ['$timeout', onFinishRender]);

	function onFinishRender($timeout){
		var directive = {};
			directive.restrict = 'A';
			directive.link = link;

		function link(scope, element, attributes){
			if (scope.$last === true) {
                $timeout(function () {
                    scope.$emit(attributes.onFinishRender);
                });
            }
		}

		return directive;
	}
})();
(function(){
	'use strict';

	angular
		.module('site')
		.directive('hoverIntent', ['$compile', '$parse', hoverIntent]);

	function hoverIntent($compile, $parse){
		var directive = {}; 
			directive.restrict= 'A';
			directive.link = link;

		function link(scope, element, attributes){

			var open = attributes.hoverIntent.split(',')[0];
			var close = attributes.hoverIntent.split(',')[1];

			var open_object = document.querySelector('.'+open);
			var close_object = document.querySelector('.'+close);


			var elm_object = element[0];
			var hover_time;

			//show dropdown on mouseover
			open_object.addEventListener('click',function(event){
				hover_time = setTimeout(function(){
					elm_object.classList.add('active');
					open_object.style.display = 'none';
					close_object.style.display = 'block';
				},300);
			});

			close_object.addEventListener('click',function(event){
				hover_time = setTimeout(function(){
					elm_object.classList.remove('active');
					close_object.style.display = 'none';
					open_object.style.display = 'block';
				},300);
			});

			//dont show dropdown if mouseover is quick
			// elm_subject.addEventListener('mouseout',function(event){
			// 	clearTimeout(hover_time);
			// });

			//hide dropdown if loose focus
			// document.addEventListener('click', function(event){
			// 	if(!isDescendant(elm_object, event.target) && !isDescendant(elm_subject, event.target)){
			// 		elm_object.classList.remove('active');
			// 	}
			// });
		}

		function isDescendant(parent, child) {
		     var node = child.parentNode;
		     while (node != null) {
		         if (node == parent) {
		             return true;
		         }
		         node = node.parentNode;
		     }
		     return false;
		}
		return directive;
	};

})();
(function(){
	'use strict'

	angular
		.module('site')
		.directive('jrange',['$timeout', jrange]);

	function jrange($timeout){
		var directive = {};
			directive.restrict = 'A';
			directive.scope = {
				'model': '='
			}
			directive.link = link;

		function link(scope, element, attributes){
			$timeout(function(){
				var range = strToRange(attributes.range);
				element.jRange({
					from: range.from,
					to: range.to,
				    step: 1,
				    width: 200,
				    showLabels: false,
				    showScale: false,
				    isRange : (attributes.jrange == 'true') ? true : false,
				    onstatechange: function(value){
				    	scope.$apply(function(){
				    		scope.model = value;
				    	});
				    },			
				});
				
				if(typeof scope.minmodel !== 'undefined' && typeof scope.maxmodel !== 'undefined'){ 
					var time;
					scope.$watch(watch, function(newValue, oldValue){
						clearTimeout(time);
						time = setTimeout(function() {
							element.jRange('setValue', newValue);
						}, 700);
					});

					function watch(){
						return scope.minmodel+','+scope.maxmodel;
					}
				}

			},1000);

		}

		function strToRange(value) {
			return {
				'from': value.split(',')[0],
				'to': value.split(',')[1]
			}
		}

		function rangeToStr(range) {
			return range.from+','+range.to;
		}

		return directive;
	}
})();
(function(){
	'use strict'

	angular
		.module('site')
		.directive('owlCarousel', ['$compile', '$parse', owlCarousel]);

	function owlCarousel($compile, $parse){
		var directive = {}
			directive.restrict= 'A';
			directive.scope = true;
			directive.compile = compile;

		function getOptions(attributes){
			var options = {}
			var optionsArray = owlOptions();

			for (var i = 0; i < optionsArray.length; i++) {
				var opt = optionsArray[i];
				if (typeof attributes[opt] !== 'undefined') {
					options[opt] = $parse(attributes[opt])();
				}
			}

			return options;
		}

		function postLink(scope, element, attributes){
			var propertyName = attributes.owlCarousel;
			var options = getOptions(attributes);

			if(typeof attributes.owlWaitfor !== 'undefined'){
				scope.$watch(watch, function(newvalue, oldvalue){
					if(typeof newvalue !== 'undefined'){
						if(newvalue.length > 0){
							scope.$on('ngRepeatFinished', function(ngRepeatFinishedEvent) {
							    element.owlCarousel(options);
							});
						}
					}
				});

				function watch(){
					return $parse(attributes.owlWaitfor)(scope);
				}
			}
			else{
				element.owlCarousel(options);
			}
		}

		function compile(element, attributes){
			return postLink;
		}

		function owlOptions(){
			return [
					'items',
					'margin',
					'loop',
					'center',
					'mouseDrag',
					'touchDrag',
					'pullDrag',
					'freeDrag',
					'merge',
					'mergeFit',
					'autoWidth',
					'startPosition',
					'URLhashListener',
					'nav',
					'navRewind',
					'navText',
					'slideBy',
					'dots',
					'dotsEach',
					'dotData',
					'lazyLoad',
					'lazyContent',
					'autoplay',
					'autoplayTimeout',
					'autoplayHoverPause',
					'smartSpeed',
					'fluidSpeed',
					'autoplaySpeed',
					'dotsSpeed',
					'dragEndSpeed',
					'callbacks',
					'responsive',
					'responsiveRefreshRate',
					'responsiveBaseElement',
					'responsiveClass',
					'video',
					'videoHeight',
					'videoWidth',
					'animateOut',
					'animateIn',
					'fallbackEasing',
					'info',
					'nestedItemSelector',
					'itemElement',
					'stageElement',
					'navContainer',
					'dotsContainer',
					'themeClass',
					'baseClass',
					'itemClass',
					'centerClass',
					'activeClass',
					'navContainerClass',
					'navClass',
					'controlsClass',
					'dotClass',
					'dotsClass',
					'autoHeightClass',
					'onInitialize',
					'onInitialized',
					'onResize',
					'onResized',
					'onRefresh',
					'onRefreshed',
					'onDrag',
					'onDragged',
					'onTranslate',
					'onTranslated',
					'onChange',
					'onChanged',
					'onStopVideo',
					'onPlayVideo',
					'onLoadLazy',
					'onLoadedLazy'
				];
		}
			
		return directive;
	}
})();

(function(){
	'use strict';

	angular
		.module('site')
		.directive('scrollBottom', ['$rootScope', scrollBottom]);

	function scrollBottom($rootScope){
		var directive = new Object();

			directive.restrict = 'A';
			directive.scope = true;
			directive.link = link;

		function link(){
			var timeout,
				scrollpos = 0;
			document.addEventListener("scroll", function(){
				//console.log(window.scrollY + window.outerHeight, document.body.clientHeight);
				if(window.scrollY + window.outerHeight > document.body.clientHeight && scrollpos < window.scrollY) {
					clearTimeout(timeout);

					timeout = setTimeout(function(){
						$rootScope.$broadcast('scroll:bottom');
					},300);
				}
				scrollpos = window.scrollY;
			});
		}

		return directive;
	}
})();
(function(){
	'use strict';

	angular
		.module('site')
		.factory('dataprovide', ['$q', '$timeout', 'localdataService', dataprovide]);

	function dataprovide($q, $timeout, localdataService){
		var factory = dataprovide;
		var data = new Object();

		function dataprovide(){
			data = localdataService.read('fakery');
			this.record = {};
			this.records = {};
			this.params = {};
			this.navigation = {};
			this.errors = {
				'has': {},
				'message': {}
			}
		}

		dataprovide.prototype.query = function(){
			var self = this,
				deferred = $q.defer(),
				result = new Array();
			
			$timeout(function(){
				for(var i=0; i<(self.params.page*5); i++){
					var item = data[i];
					if(typeof item !== 'undefined') result.push(item);
				}
				deferred.resolve(result);
			},500);
			

			return deferred.promise;
		}

		dataprovide.prototype.get = function(id){
			var self = this,
				result = new Object();
			
			for(var i=0; i<data.length; i++){
				var item = data[i];
				if(item._id == id) result = item;
			}

			return result;
		}

		dataprovide.prototype.addComment = function(id){
			var self = this;

			for(var i=0; i<data.length; i++){
				var item = data[i];
				if(item._id == id){
					var user = item.user;
					item.comments.unshift({
						'content': self.record,
						'user': user
					});
				}
			}

			localdataService.save('fakery', data);
		}

		return factory;
	}
})();
(function(){
	'use strict';

	angular
		.module('LocaldataModule',[])
		.service('localdataService', ['$q', '$rootScope', '$timeout', localdataService]);
	
	function localdataService($q, $rootScope, $timeout){
		var self = this;

		/**
		 * Check if given key is avaialable in localstorage
		 * @param  {String} key Localstorage data key
		 * @return {Boolean}     True/Flase
		 */
		self.available = function(key){
			return (localStorage.getItem(key) != null);
		}

		/**
		 * Fetch required data from server
		 * @param  {Object}   model    instance of model
		 * @param  {Function}   getter   Function in model instance to query server
		 * @param  {Function} callback A callback function to return server query response
		 */
		self.fetch = function(model, getter, callback){
			model[getter]().then(function(state){
				var result = model.records;
				callback(result);
			}).catch(function(){
				callback();
			});
		}

		/**
		 * Save data to local storage
		 * @param  {String} key  data key against which data will be saved
		 * @param  {Object} data Actual data
		 */
		self.save = function(key, data){
			localStorage.setItem(key, JSON.stringify(data));
		}

		/**
		 * Read data from localstorage
		 * @param  {String} key localstorage data key
		 * @return {Object}     Required data
		 */
		self.read = function(key){
			return JSON.parse(localStorage.getItem(key));
		}

		/**
		 * Remove data from localstorage
		 * @param  {String} key localstorage data key
		 * @return {} 
		 */
		self.remove = function(key){
			return localStorage.removeItem(key);
		}
	}
})()