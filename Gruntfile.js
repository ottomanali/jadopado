module.exports = function(grunt) {
	grunt.initConfig({
		pkg: grunt.file.readJSON('package.json'),
		uglify: {
			options: {
				mangle: false,
			},
			site: {
				files: {
					'site/app.min.js': ['site/src/concat_script.js'],
				},//files
			},//site
		},//uglify
		concat: {
			css: {
				src: ['site/assets/css/**/*.css', '!site/assets/css/concat_style.css'],
				dest: 'site/assets/css/concat_style.css',
			},//scss
			script: {
				src: ['site/src/**/*.js', '!site/src/concat_script.js', '!site/src/data.js'],
				dest: 'site/src/concat_script.js',
			},//script
		},//concat
		cssmin: {
			target: {
				files: {
					'site/style.min.css': ['site/assets/css/concat_style.css']
				}
			}
		},//cssmin
		watch: {
			css: {
				files: ['site/assets/css/**/*.css', '!site/assets/css/concat_style.css'],
				tasks: ['concat:css', 'cssmin'],
			},//css
			scripts: {
				files: ['site/src/**/*.js', '!site/src/concat_script.js', '!site/src/data.js'],
				tasks: ['concat:script', 'uglify'],
			}
		},//watch
	});

	grunt.loadNpmTasks('grunt-contrib-cssmin');
	grunt.loadNpmTasks('grunt-contrib-watch');
	grunt.loadNpmTasks('grunt-contrib-concat');
	grunt.loadNpmTasks('grunt-contrib-uglify');

	grunt.registerTask('default', ['watch']);
};