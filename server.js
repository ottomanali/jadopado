var express = require('express'),
	app 	= express();

app
	.use(express.static('./site'))
	.get('*', function(req, res){
		res.sendFile('site/index.html', { root: __dirname });
	})
	.listen(3000);